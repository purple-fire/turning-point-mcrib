/**
 * @file
 * Button on a controller.
 *
 * @copyright Copyright (c) 2019 Purple Fire Robotics
 */

#ifndef CONTROLLER_BUTTON_HPP_
#define CONTROLLER_BUTTON_HPP_

#include "main.h"

#include "Button.hpp"

class ControllerButton : public Button {
public:
    using DigitalButton = pros::controller_digital_e_t;

    ControllerButton(
            DigitalButton button,
            pros::Controller controller = pros::E_CONTROLLER_MASTER,
            bool inverted = false);

protected:
    virtual bool rawIsPressed() override;

    DigitalButton button;
    pros::Controller controller;
};

#endif  // CONTROLLER_BUTTON_HPP_

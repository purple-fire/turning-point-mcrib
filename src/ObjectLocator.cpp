/**
 * @file
 * Find relative location of objects from the vision sensor.
 *
 * @copyright Copyright (c) 2018 Purple Fire Robotics
 */

#include "ObjectLocator.hpp"

ObjectLocator::ObjectLocator(QLength width, QLength height)
        : objectWidth{width}, objectHeight{height} {}

QLength ObjectLocator::getDistance(const pros::vision_object_s_t &object) {
    // return objectWidth * FOCAL_LENGTH /
    //        std::sqrt((double)(object.width * object.height));
    return objectWidth * FOCAL_LENGTH / object.width;
}

QAngle ObjectLocator::getYaw(const pros::vision_object_s_t &object) {
    return std::atan2(object.x_middle_coord, FOV_X_SCALE) * 1_rad;
}

QAngle ObjectLocator::getPitch(const pros::vision_object_s_t &object) {
    return std::atan2(object.y_middle_coord, FOV_Y_SCALE) * 1_rad;
}

/**
 * @file
 * Generic button class.
 *
 * @copyright Copyright (c) 2019 Purple Fire Robotics
 */

#ifndef BUTTON_HPP_
#define BUTTON_HPP_

class Button {
public:
    Button(bool inverted = false, bool initPressed = false);

    virtual ~Button() = default;

    bool isPressed();

    bool isReleased();

    bool changedToPressed();

    bool changedToReleased();

protected:
    virtual bool rawIsPressed() = 0;

    bool inverted;
    bool wasPressed;
};

#endif  // BUTTON_HPP_

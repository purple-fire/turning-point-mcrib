/**
 * @file
 * PID controller.
 */

#include <cmath>

#include "PIDController.hpp"

PIDController::PIDController(Gains gains, double integralThreshold)
        : gains{gains}, integralThreshold{integralThreshold} {}

double PIDController::getTarget() const {
    return target;
}

void PIDController::setTarget(double target) {
    this->target = target;
}

double PIDController::step(double input) {
    auto error = target - input;
    auto derivative = lastError - error;

    integral += error;
    if (std::abs(integral) > integralThreshold) {
        integral = std::copysign(integralThreshold, integral);
    }

    lastError = error;
    return gains.kP * error + gains.kI * integral + gains.kD * derivative;
}

/**
 * @file
 * Single motor.
 *
 * @copyright Copyright (c) 2019 Purple Fire Robotics
 */

#include "Motor.hpp"

// Instantiate this explicitly (since it will almost definitely be used) to help
// reduce compile times
template class MotorGroup<1>;

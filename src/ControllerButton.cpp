/**
 * @file
 * Button on a controller.
 *
 * @copyright Copyright (c) 2019 Purple Fire Robotics
 */

#include "ControllerButton.hpp"

ControllerButton::ControllerButton(
        pros::controller_digital_e_t button,
        pros::Controller controller,
        bool inverted)
        : Button{inverted}, button{button}, controller{controller} {}

bool ControllerButton::rawIsPressed() {
    return controller.get_digital(button);
}

/**
 * @file
 * Global initialization functions.
 *
 * @copyright Copyright (c) 2019 Purple Fire Robotics
 */

#include "main.h"

#include "config.hpp"

/**
 * Runs initialization code. This occurs as soon as the program is started.
 *
 * All other competition modes are blocked by initialize; it is recommended
 * to keep execution time for this mode under a few seconds.
 */
void initialize() {
    pros::lcd::initialize();

    // TODO Remove this? Calibrating makes it non-absolute.
    // launcherPot.calibrate();

    leftMotors.setBrakeMode(MotorGroup<3>::BrakeMode::coast);
    rightMotors.setBrakeMode(MotorGroup<3>::BrakeMode::coast);

    launcherMotor.setGearing(Motor::Gearset::red);
    launcherMotor.setBrakeMode(Motor::BrakeMode::hold);

    launchAngleMotor.setBrakeMode(Motor::BrakeMode::hold);

    for (size_t i = 0; i < VISION_SIG_COUNT; i++) {
        frontVision.set_signature(i + 1, &visionSignatures[i]);
        pros::Vision::print_signature(visionSignatures[i]);
    }

    // blueFlagCode = frontVision.create_color_code(blueSig, greenSig);
    // redFlagCode = frontVision.create_color_code(redSig, greenSig);

    csvLog << "distance"
           << "x"
           << "y"
           << "width"
           << "height" << CsvWriter::endRow;
}

/**
 * Runs while the robot is in the disabled state of Field Management System or
 * the VEX Competition Switch, following either autonomous or opcontrol. When
 * the robot is enabled, this task will exit.
 */
void disabled() {}

/**
 * Runs after initialize(), and before autonomous when connected to the Field
 * Management System or the VEX Competition Switch. This is intended for
 * competition-specific initialization routines, such as an autonomous selector
 * on the LCD.
 *
 * This task will exit when the robot is enabled and autonomous or opcontrol
 * starts.
 */
void competition_initialize() {}

/**
 * @file
 * Generic button class.
 *
 * @copyright Copyright (c) 2019 Purple Fire Robotics
 */

#include "Button.hpp"

Button::Button(bool inverted, bool initPressed)
        : inverted{inverted}, wasPressed{initPressed} {}

bool Button::isPressed() {
    return inverted != rawIsPressed();
}

bool Button::isReleased() {
    return !isPressed();
}

bool Button::changedToPressed() {
    auto currently = isPressed();
    if (wasPressed == currently) {
        return false;
    } else {
        wasPressed = currently;
        return currently;
    }
}

bool Button::changedToReleased() {
    auto currently = isPressed();
    if (wasPressed == currently) {
        return false;
    } else {
        wasPressed = currently;
        return !currently;
    }
}

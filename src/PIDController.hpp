/**
 * @file
 * PID controller.
 */

#ifndef PID_CONTROLLER_HPP_
#define PID_CONTROLLER_HPP_

#include <limits>

class PIDController {
public:
    struct Gains {
        double kP, kI, kD;
    };

    PIDController(
            Gains gains, double threshold = std::numeric_limits<double>::max());

    double getTarget() const;
    void setTarget(double target);

    double step(double input);

private:
    Gains gains;
    double integralThreshold;

    double target = 0.0;
    double integral = 0.0;
    double lastError = 0.0;
};

#endif  // PID_CONTROLLER_HPP_

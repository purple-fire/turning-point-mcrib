/**
 * @file
 * Group of connected motors.
 *
 * @copyright Copyright (c) 2019 Purple Fire Robotics
 */

#ifndef MOTOR_GROUP_HPP_
#define MOTOR_GROUP_HPP_

#include <array>
#include <cstddef>

#include "main.h"

#include "units.hpp"

template <size_t N>
class MotorGroup {
public:
    enum class BrakeMode {
        brake = pros::E_MOTOR_BRAKE_BRAKE,
        coast = pros::E_MOTOR_BRAKE_COAST,
        hold = pros::E_MOTOR_BRAKE_HOLD,
        invalid = pros::E_MOTOR_BRAKE_INVALID,
    };

    enum class Gearset {
        red = pros::E_MOTOR_GEARSET_36,
        green = pros::E_MOTOR_GEARSET_18,
        blue = pros::E_MOTOR_GEARSET_06,
        invalid = pros::E_MOTOR_GEARSET_INVALID,
    };

    enum class EncoderUnits {
        degrees = pros::E_MOTOR_ENCODER_DEGREES,
        counts = pros::E_MOTOR_ENCODER_COUNTS,
        rotations = pros::E_MOTOR_ENCODER_ROTATIONS,
        invalid = pros::E_MOTOR_ENCODER_INVALID,
    };

    MotorGroup(std::array<pros::Motor, N> motors) : motors(motors) {}

    template <typename... Ms>
    MotorGroup(Ms... motors) : motors{motors...} {}

    int32_t move(int8_t voltage) {
        auto res = 1;
        for (auto &motor : motors) {
            if (motor.move(voltage) == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    int32_t moveVoltage(int16_t voltage) {
        auto res = 1;
        for (auto &motor : motors) {
            if (motor.move_voltage(voltage) == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    int32_t moveAbsolute(double position, int32_t velocity) {
        auto res = 1;
        for (auto &motor : motors) {
            if (motor.move_absolute(position, velocity) == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    int32_t moveAbsolute(QAngle position, QAngularSpeed velocity) {
        return moveAbsolute(
                position.convert(getEncoderUnits()), ConvertTo(velocity, rpm));
    }

    int32_t moveAbsolute(QAngle position) {
        return moveAbsolute(position, getGearsetRPM() * 1_rpm);
    }

    int32_t moveRelative(double position, int32_t velocity) {
        auto res = 1;
        for (auto &motor : motors) {
            if (motor.move_relative(position, velocity) == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    int32_t moveRelative(QAngle position, QAngularSpeed velocity) {
        return moveRelative(
                position.convert(getEncoderUnits()), ConvertTo(velocity, rpm));
    }

    int32_t moveRelative(QAngle position) {
        return moveRelative(position, getGearsetRPM() * 1_rpm);
    }

    int32_t moveVelocity(int16_t velocity) {
        auto res = 1;
        for (auto &motor : motors) {
            if (motor.move_velocity(velocity) == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    int32_t moveVelocity(QAngularSpeed velocity) {
        return moveVelocity(ConvertTo(velocity, rpm));
    }

    int32_t modifyProfiledVelocity(int32_t velocity) {
        auto res = 1;
        for (auto &motor : motors) {
            if (motor.modify_profiled_velocity(velocity) == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    int32_t modifyProfiledVelocity(QAngularSpeed velocity) {
        return modifyProfiledVelocity(ConvertTo(velocity, rpm));
    }

    int32_t getTargetVelocity() const {
        return motors[0].get_target_velocity();
    }

    int32_t getActualVelocity() const {
        return motors[0].get_actual_velocity();
    }

    int32_t getDirection() const { return motors[0].get_direction(); }

    int32_t getRawPosition(uint32_t *timestamp) const {
        return motors[0].get_raw_position(timestamp);
    }

    // TODO Average values from all motors for these

    int32_t getTorque() const { return motors[0].get_torque(); }

    double getPower() const { return motors[0].get_power() * N; }

    double getVoltage() const { return motors[0].get_voltage(); }

    int32_t getCurrentDraw() { return motors[0].get_current_draw() * N; }

    double getEfficiency() const { return motors[0].get_efficiency(); }

    double getTemperature() const { return motors[0].get_temperature(); }

    uint32_t getFaults() const {
        auto res = 0;
        for (auto &motor : motors) {
            res |= motor.get_faults();
        }

        return res;
    }

    uint32_t getFlags() const {
        auto res = 0;
        for (auto &motor : motors) {
            res |= motor.get_flags();
        }

        return res;
    }

    int32_t getZeroPositionFlag() const {
        // All motors must be in zero position
        for (auto &motor : motors) {
            auto retval = motor.get_zero_position_flag();
            if (retval == PROS_ERR) {
                return PROS_ERR;
            } else if (!retval) {
                return false;
            }
        }

        return true;
    }

    int32_t isStopped() const {
        // All motors must be stopped
        for (auto &motor : motors) {
            auto retval = motor.is_stopped();
            if (retval == PROS_ERR) {
                return PROS_ERR;
            } else if (!retval) {
                return false;
            }
        }

        return true;
    }

    int32_t isOverCurrent() const {
        // Any motor can be over current
        for (auto &motor : motors) {
            auto retval = motor.is_over_current();
            if (retval == PROS_ERR) {
                return PROS_ERR;
            } else if (retval) {
                return true;
            }
        }

        return false;
    }

    int32_t isOverTemp() const {
        // Any motor can be over temp
        for (auto &motor : motors) {
            auto retval = motor.is_over_temp();
            if (retval == PROS_ERR) {
                return PROS_ERR;
            } else if (retval) {
                return true;
            }
        }

        return false;
    }

    BrakeMode getBrakeMode() const {
        return static_cast<BrakeMode>(motors[0].get_brake_mode());
    }

    Gearset getGearing() const {
        // TODO Factor in external gearing
        return static_cast<Gearset>(motors[0].get_gearing());
    }

    int32_t getGearsetRPM() const {
        switch (getGearing()) {
            case Gearset::red:
                return 100;
            case Gearset::green:
                return 200;
            case Gearset::blue:
                return 600;
            default:
                return 0;
        }
    }

    QAngle getEncoderUnits() const {
        // TODO Check that all motors are the same?
        switch (motors[0].get_encoder_units()) {
            case pros::E_MOTOR_ENCODER_DEGREES:
                return 1.0_deg;
            case pros::E_MOTOR_ENCODER_COUNTS:
                // TODO Is this unit correct?
                return 3600_deg;
            case pros::E_MOTOR_ENCODER_ROTATIONS:
                return 360_deg;
            default:
                return 0_deg;
        }
    }

    int32_t isReversed() const {
        auto res = 1;
        for (auto &motor : motors) {
            if (motor.is_reversed() == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    int32_t getVoltageLimit() const {
        // TODO Get minimum?
        return motors[0].get_voltage_limit();
    }

    int32_t getCurrentLimit() const {
        // TODO Get minimum?
        return motors[0].get_current_limit();
    }

    int32_t setBrakeMode(BrakeMode mode) {
        auto res = 1;
        auto prosMode = static_cast<pros::motor_brake_mode_e_t>(mode);
        for (auto &motor : motors) {
            if (motor.set_brake_mode(prosMode) == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    int32_t setGearing(Gearset set) {
        auto res = 1;
        auto prosSet = static_cast<pros::motor_gearset_e_t>(set);
        for (auto &motor : motors) {
            if (motor.set_gearing(prosSet) == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    int32_t setEncoderUnits(EncoderUnits units) {
        auto res = 1;
        auto prosUnits = static_cast<pros::motor_encoder_units_e_t>(units);
        for (auto &motor : motors) {
            if (motor.set_encoder_units(prosUnits) == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    int32_t setReversed(bool reverse = true) {
        auto res = 1;
        for (auto &motor : motors) {
            if (motor.set_reversed(reverse) == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    int32_t setVoltageLimit(int32_t limit) {
        auto res = 1;
        for (auto &motor : motors) {
            if (motor.set_voltage_limit(limit) == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    int32_t setCurrentLimit(int32_t limit) {
        auto res = 1;
        for (auto &motor : motors) {
            if (motor.set_current_limit(limit) == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    int32_t setZeroPosition(double position) {
        auto res = 1;
        for (auto &motor : motors) {
            if (motor.set_zero_position(position) == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    int32_t tarePosition() {
        auto res = 1;
        for (auto &motor : motors) {
            if (motor.tare_position() == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

private:
    // TODO Store external gearing

    std::array<pros::Motor, N> motors;
};

#endif  // MOTOR_GROUP_HPP_

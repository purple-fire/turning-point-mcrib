/**
 * @file
 * Find relative location of objects from the vision sensor.
 *
 * @copyright Copyright (c) 2018 Purple Fire Robotics
 */

#ifndef OBJECT_LOCATOR_HPP_
#define OBJECT_LOCATOR_HPP_

#include <cmath>

#include "main.h"
#include "units.hpp"

class ObjectLocator {
public:
    ObjectLocator(QLength width, QLength height);

    QLength getDistance(const pros::vision_object_s_t &object);

    QAngle getYaw(const pros::vision_object_s_t &object);

    QAngle getPitch(const pros::vision_object_s_t &object);

    const double FOCAL_LENGTH = 290.0;

    // TODO Find the actual FOV angles
    const double FOV_X_SCALE =
            VISION_FOV_WIDTH / 2 / std::tan(30.0 / 2.0 * M_PI / 180.0);
    const double FOV_Y_SCALE =
            VISION_FOV_HEIGHT / 2 / std::tan(20.0 / 2.0 * M_PI / 180.0);

private:
    QLength objectWidth;
    QLength objectHeight;
};

#endif  // OBJECT_LOCATOR_HPP_

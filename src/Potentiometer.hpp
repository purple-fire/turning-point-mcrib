/**
 * @file
 * Potentiometer.
 *
 * @copyright Copyright (c) 2019 Purple Fire Robotics
 */

#ifndef POTENTIOMETER_HPP_
#define POTENTIOMETER_HPP_

#include "main.h"

#include "units.hpp"

class Potentiometer : public pros::ADIAnalogIn {
public:
    Potentiometer(uint8_t port);

    QAngle getAngle() const;
};

#endif  // POTENTIOMETER_HPP_

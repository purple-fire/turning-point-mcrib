/**
 * @file
 * Vision tracking with filters on which objects are reported.
 *
 * @copyright Copyright (c) 2019 Purple Fire Robotics
 */

#include "FilteredVision.hpp"

FilteredVision::FilteredVision(uint8_t port, pros::vision_zero_e_t zeroPoint)
        : Vision(port, zeroPoint) {}

pros::vision_object_s_t FilteredVision::getBySig(
        uint32_t sig,
        std::function<bool(const pros::vision_object_s_t &)> filter) {
    pros::vision_object_s_t object;
    if (readBySig(sig, 1, filter, &object) != 1) {
        object.signature = 255;
    }
    return object;
}

int32_t FilteredVision::readBySig(
        uint32_t sig,
        uint32_t count,
        std::function<bool(const pros::vision_object_s_t &)> filter,
        pros::vision_object_s_t objectArr[]) {
    auto objects = readBySig(sig, count, filter);
    for (auto i = 0; i < objects.size(); ++i) {
        objectArr[i] = objects[i];
    }
    return objects.size();
}

std::vector<pros::vision_object_s_t> FilteredVision::readBySig(
        uint32_t sig,
        uint32_t count,
        std::function<bool(const pros::vision_object_s_t &)> filter) {
    auto offset = 0;
    auto capacity = 1;
    std::vector<pros::vision_object_s_t> objects;

    buffer.resize(capacity);
    while (true) {
        auto readCount = read_by_sig(offset, sig, capacity, buffer.data());
        if (readCount == PROS_ERR) {
            objects.clear();
            break;
        } else if (readCount == 0) {
            break;
        }
        for (auto i = 0; i < readCount; ++i) {
            if (filter(buffer[i])) {
                objects.push_back(buffer[i]);
                if (objects.size() == count) {
                    return objects;
                }
            }
        }

        offset += readCount;
        capacity *= 2;
        buffer.resize(capacity);
    }

    return objects;
}

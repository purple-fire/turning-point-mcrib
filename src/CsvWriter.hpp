/**
 * @file
 * CSV writer stream.
 *
 * @copyright Copyright (c) 2019 Zach Peltzer
 */

#ifndef CSV_WRITER_HPP_
#define CSV_WRITER_HPP_

#include <iostream>
#include <type_traits>

/**
 * Stream which writes data in CSV format.
 *
 * Commas are automatically isnerted between fields. Rows are terminated by
 * sending CsvWriter::endRow.
 *
 * This writer complies with the RFC 4180 CSV specification.
 */
class CsvWriter {
public:
    /**
     * Flags for special output.
     */
    enum class Flags {
        /**
         * Ends a CSV row.
         */
        endRow,
    };

    /**
     * Alias to Flags::endRow for easier access.
     */
    static constexpr Flags endRow = Flags::endRow;

    /**
     * Constructs a CSV writer stream.
     *
     * @param output stream to output to
     */
    CsvWriter(std::ostream &output);

    /**
     * Writes a special flag to the writer.
     *
     * @param flag flag to send
     * @return the writer
     */
    CsvWriter &operator<<(Flags flag);

    /**
     * Writes a string to the stream.
     *
     * The string is quoted if necessary. If the string cannot be properly
     * quoted, an exception will be thrown.
     *
     * @param val string to write
     * @return the writer
     */
    CsvWriter &operator<<(const std::string &val);

    /**
     * Writes a numeric value to the stream.
     *
     * @tparam T type of value to write
     * @param val value to write
     * @return the writer
     */
    template <
            typename T,
            typename = typename std::
                    enable_if<std::is_arithmetic<T>::value, T>::type>
    CsvWriter &operator<<(T val) {
        handleStart();
        output << val;
        return *this;
    }

private:
    /**
     * Writes a comma to the output stream if not at the start of a row.
     *
     * This is called before outputting data in the << overloads.
     */
    void handleStart();

    /**
     * Whether the writer is currently at the start of a row.
     */
    bool atStart = true;

    /**
     * Stream to write to.
     */
    std::ostream &output;
};

#endif // CSV_WRITER_HPP_

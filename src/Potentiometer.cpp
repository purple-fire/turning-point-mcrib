/**
 * @file
 * Potentiometer.
 *
 * @copyright Copyright (c) 2019 Purple Fire Robotics
 */

#include "Potentiometer.hpp"

Potentiometer::Potentiometer(uint8_t port) : ADIAnalogIn{port} {}

QAngle Potentiometer::getAngle() const {
    return get_value() / 4095.0 * 250.0_deg;
}

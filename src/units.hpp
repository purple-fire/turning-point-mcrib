/**
 * @file
 * Include units from OkapiLib.
 *
 * @copyright Copyright (c) 2019 Purple Fire Robotics
 */

#ifndef UNITS_HPP_
#define UNITS_HPP_

#include "okapi/api/units/QAcceleration.hpp"
#include "okapi/api/units/QAngle.hpp"
#include "okapi/api/units/QAngularAcceleration.hpp"
#include "okapi/api/units/QAngularJerk.hpp"
#include "okapi/api/units/QAngularSpeed.hpp"
#include "okapi/api/units/QArea.hpp"
#include "okapi/api/units/QForce.hpp"
#include "okapi/api/units/QFrequency.hpp"
#include "okapi/api/units/QJerk.hpp"
#include "okapi/api/units/QLength.hpp"
#include "okapi/api/units/QMass.hpp"
#include "okapi/api/units/QPressure.hpp"
#include "okapi/api/units/QSpeed.hpp"
#include "okapi/api/units/QTime.hpp"
#include "okapi/api/units/QTorque.hpp"
#include "okapi/api/units/QVolume.hpp"

// Un-namespace the types and literals

using QAcceleration = okapi::QAcceleration;
using QAngle = okapi::QAngle;
using QAngularAcceleration = okapi::QAngularAcceleration;
using QAngularJerk = okapi::QAngularJerk;
using QAngularSpeed = okapi::QAngularSpeed;
using QArea = okapi::QArea;
using QForce = okapi::QForce;
using QFrequency = okapi::QFrequency;
using QJerk = okapi::QJerk;
using QLength = okapi::QLength;
using QMass = okapi::QMass;
using QPressure = okapi::QPressure;
using QSpeed = okapi::QSpeed;
using QTime = okapi::QTime;
using QTorque = okapi::QTorque;
using QVolume = okapi::QVolume;

using namespace okapi::literals;

#endif  // UNITS_HPP_

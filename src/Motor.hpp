/**
 * @file
 * Single motor
 *
 * @copyright Copyright (c) 2019 Purple Fire Robotics
 */

#ifndef MOTOR_HPP_
#define MOTOR_HPP_

#include "MotorGroup.hpp"

using Motor = MotorGroup<1>;

extern template class MotorGroup<1>;

#endif  // MOTOR_HPP_

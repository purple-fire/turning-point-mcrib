/**
 * @file
 * Global configuration
 *
 * @copyright Copyright (c) 2019 Purple Fire Robotics
 */

#include "config.hpp"

using namespace pros::literals;

MotorGroup<3> leftMotors{1_rmtr, 2_rmtr, 3_rmtr};
MotorGroup<3> rightMotors{4_mtr, 5_mtr, 6_mtr};

Motor intakeMotor{16_mtr};

Motor launcherMotor{9_mtr};

Motor launchAngleMotor{10_mtr};

Potentiometer launcherPot{'A'};

FilteredVision frontVision{11, pros::E_VISION_ZERO_CENTER};

// These are initialized in initialize() since they depend on normal color
// signatures
pros::vision_color_code_t blueFlagCode;
pros::vision_color_code_t redFlagCode;

ObjectLocator flagLocator{5.125_in, 5.5_in};
ObjectLocator ballLocator{2.75_in, 2.75_in};

CsvWriter csvLog{std::cout};

#include "signatures.inc"

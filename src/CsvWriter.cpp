/**
 * @file
 * CSV writer stream.
 *
 * @copyright Copyright (c) 2019 Zach Peltzer
 */

#include "CsvWriter.hpp"

CsvWriter::CsvWriter(std::ostream &output) : output(output) {}

void CsvWriter::handleStart() {
    if (atStart) {
        atStart = false;
    } else {
        output << ',';
    }
}

CsvWriter &CsvWriter::operator<<(const std::string &s) {
    if (s.find_first_of(",\"\r\n") != std::string::npos) {
        handleStart();

        // Print character-wise to escape quotes
        output << '"';
        for (auto c : s) {
            if (c == '"') {
                output << "\"\"";
            } else {
                output << c;
            }
        }
        output << '"';
    } else {
        handleStart();
        output << s;
    }
    return *this;
}

CsvWriter &CsvWriter::operator<<(Flags flag) {
    switch (flag) {
    case Flags::endRow:
        output << "\r\n";
        atStart = true;
        break;
    }

    return *this;
}

/**
 * @file
 * Entry point for opcontrol.
 *
 * @copyright Copyright (c) 2019 Purple Fire Robotics
 */

#include "main.h"

#include "config.hpp"

#include "ControllerButton.hpp"

pros::Controller master{pros::E_CONTROLLER_MASTER};

ControllerButton intakeIn{pros::E_CONTROLLER_DIGITAL_X};
ControllerButton intakeOut{pros::E_CONTROLLER_DIGITAL_B};

ControllerButton launcherFire{pros::E_CONTROLLER_DIGITAL_R1};

ControllerButton launcherAngleRaise{pros::E_CONTROLLER_DIGITAL_L1};
ControllerButton launcherAngleLower{pros::E_CONTROLLER_DIGITAL_L2};

ControllerButton driveRegular{pros::E_CONTROLLER_DIGITAL_UP};
ControllerButton driveReverse{pros::E_CONTROLLER_DIGITAL_DOWN};

ControllerButton incrDist{pros::E_CONTROLLER_DIGITAL_RIGHT};
ControllerButton decrDist{pros::E_CONTROLLER_DIGITAL_LEFT};
ControllerButton logButton{pros::E_CONTROLLER_DIGITAL_Y};

static void printDriveState(bool reversed) {
    master.print(0, 0, "%s drive ", reversed ? "Reversed" : "Regular");
    pros::lcd::print(0, "%s drive ", reversed ? "Reversed" : "Regular");
}

static void printRealDist(QLength dist) {
    master.print(1, 0, "Distance: %d in ", (int)ConvertTo(dist, in));
    pros::lcd::print(1, "Distance: %d in ", (int)ConvertTo(dist, in));
}

/**
 * Runs the operator control code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the operator
 * control mode.
 *
 * If no competition control is connected, this function will run immediately
 * following initialize().
 *
 * If the robot is disabled or communications is lost, the
 * operator control task will be stopped. Re-enabling the robot will restart the
 * task, not resume it from where it left off.
 */
void opcontrol() {
    bool driveFlipped = true;
    int8_t intakePower = 0;
    QLength dist = 24_in;

    master.clear();
    printDriveState(driveFlipped);
    printRealDist(dist);
    while (true) {
        // Intake

        if (intakeIn.changedToPressed()) {
            intakePower = intakePower == 0 ? INTAKE_ON_POWER : 0;
        }

        if (intakeOut.changedToPressed()) {
            intakePower = intakePower == 0 ? -INTAKE_ON_POWER : 0;
        }

        intakeMotor.move(intakePower);

        // Launcher angle

        if (launcherAngleRaise.isPressed()) {
            launchAngleMotor.moveVelocity(-50_rpm);
        } else if (launcherAngleLower.isPressed()) {
            launchAngleMotor.moveVelocity(50_rpm);
        } else {
            launchAngleMotor.moveVelocity(0);
        }

        // Launcher

        launcherMotor.move(launcherFire.isPressed() ? LAUNCHER_FIRE_POWER : 0);

        // Chassis

        if (driveRegular.changedToPressed()) {
            driveFlipped = false;
            master.set_text(0, 0, "Regular drive");
        }

        if (driveReverse.changedToPressed()) {
            driveFlipped = true;
            master.set_text(0, 0, "Reversed drive");
        }

        auto fbJoy = master.get_analog(pros::E_CONTROLLER_ANALOG_LEFT_Y);
        auto rlJoy = master.get_analog(pros::E_CONTROLLER_ANALOG_RIGHT_X);
        if (driveFlipped) {
            fbJoy = -fbJoy;
        }

        leftMotors.move(fbJoy - rlJoy);
        rightMotors.move(fbJoy + rlJoy);

        // Logging

        if (incrDist.changedToPressed()) {
            dist += 1_in;
            printRealDist(dist);
        } else if (decrDist.changedToPressed()) {
            dist -= 1_in;
            printRealDist(dist);
        }

        if (logButton.changedToPressed()) {
            double x = 0.0, y = 0.0, w = 0.0, h = 0.0;
            int count = 0;
            for (auto i = 0; i < 50; ++i) {
                auto object = frontVision.getBySig(
                        redSig,
                        [](const pros::vision_object_s_t &o) { return true; });
                if (object.signature == redSig) {
                    x += object.x_middle_coord;
                    y += object.x_middle_coord;
                    w += object.width;
                    h += object.height;
                    ++count;
                } else {
                    std::cout << "Object " << object.signature << std::endl;
                }
                pros::delay(10);
            }

            if (count == 0) {
                std::cout << "No samples" << std::endl;
            } else {
                x /= count;
                y /= count;
                w /= count;
                h /= count;
                csvLog << ConvertTo(dist, in) << x << y << w << h
                       << CsvWriter::endRow;
            }
        }

        pros::delay(10);
    }
}

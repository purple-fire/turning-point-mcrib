/**
 * @file
 * Global configuration
 *
 * @copyright Copyright (c) 2019 Purple Fire Robotics
 */

#ifndef CONFIG_HPP_
#define CONFIG_HPP_

#include "api.h"

#include "CsvWriter.hpp"
#include "FilteredVision.hpp"
#include "Motor.hpp"
#include "MotorGroup.hpp"
#include "ObjectLocator.hpp"
#include "Potentiometer.hpp"

constexpr int8_t INTAKE_ON_POWER = 127;

constexpr int8_t LAUNCHER_FIRE_POWER = -127;

constexpr size_t VISION_SIG_COUNT = 7;

extern MotorGroup<3> leftMotors;
extern MotorGroup<3> rightMotors;

extern Motor intakeMotor;

extern Motor launcherMotor;

extern Motor launchAngleMotor;

extern Potentiometer launcherPot;

constexpr uint32_t yellowSig = 1;
constexpr uint32_t blueSig = 2;
constexpr uint32_t redSig = 3;
constexpr uint32_t greenSig = 4;
constexpr uint32_t purpleSig = 7;

extern pros::vision_color_code_t blueFlagCode;
extern pros::vision_color_code_t redFlagCode;

extern ObjectLocator flagLocator;
extern ObjectLocator ballLocator;

extern FilteredVision frontVision;

extern pros::vision_signature_s_t visionSignatures[VISION_SIG_COUNT];

extern CsvWriter csvLog;

#endif  // CONFIG_HPP_

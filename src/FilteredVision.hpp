/**
 * @file
 * Vision tracking with filters on which objects are reported.
 *
 * @copyright Copyright (c) 2019 Purple Fire Robotics
 */

#ifndef FILTERED_VISION_HPP_
#define FILTERED_VISION_HPP_

#include <functional>
#include <vector>

#include "main.h"

class FilteredVision : protected pros::Vision {
public:
    FilteredVision(
            uint8_t port,
            pros::vision_zero_e_t zeroPoint = pros::E_VISION_ZERO_TOPLEFT);

    pros::vision_object_s_t getBySig(
            uint32_t sig,
            std::function<bool(const pros::vision_object_s_t &)> filter);

    int32_t readBySig(
            uint32_t sig,
            uint32_t count,
            std::function<bool(const pros::vision_object_s_t &)> filter,
            pros::vision_object_s_t objectArr[]);

    std::vector<pros::vision_object_s_t> readBySig(
            uint32_t sig,
            uint32_t count,
            std::function<bool(const pros::vision_object_s_t &)> filter);

    // Expose some functions

    using pros::Vision::create_color_code;
    using pros::Vision::get_white_balance;
    using pros::Vision::set_auto_white_balance;
    using pros::Vision::set_signature;
    using pros::Vision::set_white_balance;

private:
    std::vector<pros::vision_object_s_t> buffer;
};

#endif  // FILTERED_VISION_HPP_

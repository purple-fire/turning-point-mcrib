################################################################################
######################### User configurable parameters #########################
# filename extensions
CEXTS:=c
ASMEXTS:=s S
CXXEXTS:=cpp c++ cc

# probably shouldn't modify these, but you may need them below
ROOT=.
FWDIR:=$(ROOT)/firmware
BINDIR=$(ROOT)/bin
SRCDIR=$(ROOT)/src
INCDIR=$(ROOT)/include

WARNFLAGS+=
EXTRA_CFLAGS=
EXTRA_CXXFLAGS=

# Set to 1 to enable hot/cold linking
USE_PACKAGE:=0

# Set this to 1 to add additional rules to compile your project as a PROS library template
IS_LIBRARY:=0
# TODO: CHANGE THIS!
LIBNAME:=libbest
VERSION:=1.0.0
# EXCLUDE_SRC_FROM_LIB= $(SRCDIR)/unpublishedfile.c
# this line excludes opcontrol.c and similar files
EXCLUDE_SRC_FROM_LIB+=$(foreach file, $(SRCDIR)/opcontrol $(SRCDIR)/initialize $(SRCDIR)/autonomous,$(foreach cext,$(CEXTS),$(file).$(cext)) $(foreach cxxext,$(CXXEXTS),$(file).$(cxxext)))

# files that get distributed to every user (beyond your source archive) - add
# whatever files you want here. This line is configured to add all header files
# that are in the the include directory get exported
TEMPLATE_FILES=$(INCDIR)/**/*.h $(INCDIR)/**/*.hpp

.DEFAULT_GOAL=quick

################################################################################
################################################################################
########## Nothing below this line should be edited by typical users ###########
-include ./config.mk
-include ./common.mk

PYTHON?=python3
EXTRACTSIG:=$(PYTHON) $(ROOT)/extract_signatures.py

DOCDIR:=$(ROOT)/doc
DOXYGEN:=doxygen

SIGSRC=$(ROOT)/vision.vex
SIG_CSRC=$(SRCDIR)/signatures.inc
CONFIG_OBJ=$(BINDIR)/config.cpp.o
SIG_VARIABLE=visionSignatures

$(CONFIG_OBJ): $(SIG_CSRC)

$(SIG_CSRC): $(SIGSRC)
	$(EXTRACTSIG) $(SIG_VARIABLE) $< $@

doc:
	$(DOXYGEN)

clean-doc:
	@rm -rf $(DOCDIR)

clean-sigsrc:
	@rm -f $(SIG_CSRC)

clean: clean-doc clean-sigsrc

.PHONY: doc clean-doc clean-sigsrc


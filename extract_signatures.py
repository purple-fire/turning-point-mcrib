#!/usr/bin/env python3

import json
import sys

# I think this is fixed for all .vex files
JSON_OFFSET = 0x200


def indent_spaces(depth, indent=4):
    return ' ' * (depth * indent)


def cpp_sig(id, signature, depth=0, indent=4):
    base = indent_spaces(depth, indent)
    fields = indent_spaces(depth + 1, indent)

    yield base + '{\n'

    yield fields + f'{id},\n'
    yield fields + '{},\n'
    yield fields + f'{signature["range"]},\n'
    params = signature['parameters']
    for k in ['uMin', 'uMax', 'uMean', 'vMin', 'vMax', 'vMean', 'rgb', 'type']:
        yield fields + f'{int(params[k])},\n'

    yield base + '}'


def cpp_sig_list(name, signatures, depth=0, indent=4):
    base = indent_spaces(depth, indent)

    yield base + f'pros::vision_signature_s_t {name}[] = {{\n'

    for idx, sig in enumerate(signatures):
        yield from cpp_sig(idx+1, sig, depth=depth+1, indent=indent)
        yield ',\n'

    yield base + '};\n'


def main():
    if len(sys.argv) < 3:
        print(f'usage: {sys.argv[0]} VAR_NAME FILE [OUTPUT]')
        sys.exit(-1)

    with open(sys.argv[2], 'rb') as f:
        f.seek(JSON_OFFSET, 0)
        raw = f.read()

    raw = raw.split(b'\x00')[0]

    data = json.loads(raw)

    signatures = [c['data']['signatures'] for c in data['components']
                  if c['class'] == 'vex::vision'][0]

    outfile = open(sys.argv[3], 'w') if len(sys.argv) >= 4 else sys.stdout
    for line in cpp_sig_list(sys.argv[1], signatures):
        print(line, end='', file=outfile)

    outfile.close()


if __name__ == '__main__':
    main()
